#!/bin/bash

PYLEARN_PATH=~/Development/pylearn2/pylearn2
WORK_PATH=.
MNIST_PATH=$WORK_PATH/mnist
NEWS_PATH=$WORK_PATH/newsgroups
TRANSCODER_PATH=$WORK_PATH/transcoder

#MNIST CLASSIFIER PKL
export MNIST_CLASS=$WORK_PATH/classifier-conv.pkl

#TELL PYLEARN2 WHERE MNIST FOLDER IS
export PYLEARN2_DATA_PATH=.
export PYTHONPATH=$WORK_PATH/pylearn2:$PYTHONPATH
export PATH=$WORK_PATH/pylearn2:$PATH

#MNIST AUTOENCODER
python $PYLEARN_PATH/scripts/train.py $MNIST_PATH/yaml/rbm1.yaml
export RBM_MNIST_1=$MNIST_PATH/yaml/rbm1.pkl

python $PYLEARN_PATH/scripts/train.py $MNIST_PATH/yaml/rbm2.yaml
export RBM_MNIST_2=$MNIST_PATH/yaml/rbm2.pkl

python $PYLEARN_PATH/scripts/train.py $MNIST_PATH/yaml/rbm3.yaml
export RBM_MNIST_3=$MNIST_PATH/yaml/rbm3.pkl

python $PYLEARN_PATH/scripts/train.py $MNIST_PATH/yaml/rbm4-i4.yaml
export RBM_MNIST_4=$MNIST_PATH/yaml/rbm4-i4.pkl

python $PYLEARN_PATH/scripts/train.py $MNIST_PATH/yaml/fine-tuning.yaml
export MNIST_DAE=$MNIST_PATH/yaml/fine-tuning.pkl

#NEWSGROUPS AUTOENCODER
#FEATURE EXTRACTION
python $NEWS_PATH/features.py -b -f $NEWS_PATH
export NEWS_DATA=$NEWS_PATH/X_train.npy
export NEWS_DATA_LABELS=$NEWS_PATH/y_train.npy
export NEWS_TEST=$NEWS_PATH/X_test.npy
export NEWS_TEST_LABELS=$NEWS_PATH/y_test.npy

python $PYLEARN_PATH/scripts/train.py $NEWS_PATH/yaml/rbm1.yaml
export RBM_NEWS_1=$NEWS_PATH/yaml/rbm1.pkl

python $PYLEARN_PATH/scripts/train.py $NEWS_PATH/yaml/rbm2.yaml
export RBM_NEWS_2=$NEWS_PATH/yaml/rbm2.pkl

python $PYLEARN_PATH/scripts/train.py $NEWS_PATH/yaml/rbm3.yaml
export RBM_NEWS_3=$NEWS_PATH/yaml/rbm3.pkl

python $PYLEARN_PATH/scripts/train.py $NEWS_PATH/yaml/rbm4-o6.yaml
export RBM_NEWS_4=$NEWS_PATH/yaml/rbm4-o6.pkl

python $PYLEARN_PATH/scripts/train.py $NEWS_PATH/yaml/fine-tuning.yaml
export NEWS_DAE=$NEWS_PATH/yaml/fine-tuning.pkl

#LEARN TRANSCODER
python $PYLEARN_PATH/scripts/train.py $TRANSCODER_PATH/transcoder.yaml
export TRANSCODER=$TRANSCODER_PATH/transcoder.pkl

#PRINT ACCURACY
python $WORK_PATH/test-transcoder.py -td $NEWS_TEST -tl $NEWS_TEST_LABELS -m $MNIST_CLASS -e $NEWS_DAE -d $MNIST_DAE -t $TRANSCODER



