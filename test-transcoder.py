from pylearn2.space import VectorSpace, Conv2DSpace
from pylearn2.utils import serial
from theano import shared
import argparse
import cv2
import networkx as nx
import numpy as np
import pylearn2.datasets.mnist as mnist
import theano
import theano.tensor as T
import subprocess
import os

def compute_accuracy(test_data_src, test_labels_src, output_labels, mnist_classifier, image_dir=None, transcoder=None, input_labels=None, encoder=None, decoder=None, get_accuracy=False):

	test_data = np.load(test_data_src)
	test_labels = np.load(test_labels_src)
	if not image_dir is None:
		image_dir = image_dir + '/' + transcoder.split('/')[-1][:-4]
		if not os.path.exists(image_dir):
			os.makedirs(image_dir)
	else:
		print 'No Image Dir'

	if input_labels is None:
		labels = sorted(list(set(test_labels.tolist())))
		input_labels = max(labels) + 1
	else:
		labels = np.arange(input_labels)

	if get_accuracy:
		 model_classifier = serial.load(mnist_classifier)
	
	# ENCODER
	if not encoder is None:
		model_newsgroups = serial.load(encoder)
		all_features = model_newsgroups(shared(test_data, 'X'))
		encoded_newsgroups = all_features
	else:
		encoded_newsgroups = shared(test_data, 'X')

	# TRANSCODER
	if transcoder is None:
		transcoded_features = encoded_newsgroups
	else:
		transcoder = serial.load(transcoder)
		transcoded_features = transcoder.fprop(encoded_newsgroups)

#	strt = ''
# 	for t,o in zip(transcoded_features.eval(),test_labels) :
#		strt += str(o) + ' ' + ' '.join(['%f' % tt for tt in t]) + '\n'
#	print strt
	
	# DECODER
	if not decoder is None:
		model_mnist = serial.load(decoder)
		decoded_mnist = model_mnist.reconstruct_from_h(transcoded_features)
	else:
		decoded_mnist = transcoded_features		

	recs = decoded_mnist.eval()
	confusion_matrix = np.zeros((input_labels, output_labels))

	_input = T.tensor4()
	if get_accuracy:
		f = theano.function([_input], T.argmax(model_classifier.fprop(_input), axis = 1))
	for label in range(len(labels)):
		mnist_images = recs[np.where(test_labels == labels[label])[0]].astype(theano.config.floatX)
	
		if get_accuracy:
			n = f(mnist_images.transpose().reshape((1,28,28, mnist_images.shape[0]))).astype('int')
			confusion_matrix[label, :] += np.bincount(n, minlength=output_labels)

		if not image_dir is None:
			for i in range(mnist_images.shape[0]):
				cv2.imwrite('%s/cat-%s-%s.png' % (image_dir, label, i) , 255*(1.0 - mnist_images[i].reshape(28, 28)))
	
	if not get_accuracy:
		return

	G = nx.DiGraph()
	for i in range(input_labels):
		for j in range(output_labels):
			G.add_edge('i'+str(i), 'o'+str(j), weight=confusion_matrix[i, j]+1)

	associations = nx.max_weight_matching(G)
	chosen_values = np.zeros((input_labels, output_labels))

	for i in range(input_labels):
		k = 'i'+ str(i)		
		if k in associations:
			chosen_values[i, int(associations[k][1:])] = 1

	return confusion_matrix, chosen_values
	print ' '.join(['r%s:c%s' % (k[1:], associations[k][1:]) for k in sorted(associations.keys()) if k[0] == 'i'])

def get_all_accuracies(confusion_matrix, chosen_values):
	num = np.sum(confusion_matrix * chosen_values, axis=1)
	den =  np.sum(confusion_matrix, axis=1)
	overall_acc = 100. * np.sum(num) / np.sum(den)
	num[den != 0] = 100. * num[den != 0] / den[den != 0]
	classes_acc = num
	mean_acc = np.mean(classes_acc)
	return overall_acc, mean_acc, classes_acc

def main(test_data_src, test_labels_src, mnist_classifier, decoder, encoder, transcoder, image_dir=None):
	confusion_matrix, chosen_values = compute_accuracy(test_data_src=test_data_src, test_labels_src=test_labels_src, mnist_classifier=mnist_classifier, image_dir=image_dir, output_labels=10, transcoder=transcoder, decoder=decoder, encoder=encoder,get_accuracy=True)

	overall_acc, mean_acc, classes_acc = get_all_accuracies(confusion_matrix, chosen_values)
	
	np.set_printoptions(formatter={'all':lambda x: str(int(x))})
	print 'Confusion Matrix'
	print confusion_matrix
	print 'Chosen Values'
	print confusion_matrix * chosen_values
	for i in range(classes_acc.shape[0]):
		print 'Class %d: %.2f%%' % (i, classes_acc[i])

	print 'Mean Accuracy: %.2f%%' % (mean_acc)
	print 'Overall Accuracy: %.2f%%' % (overall_acc)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Compute Accuracy of Transcoder.')
	parser.add_argument('-td', '--test_data', help='Test data file.', required=True)
	parser.add_argument('-tl', '--test_data_labels', help='Test data labels file.', required=True)
	parser.add_argument('-m', '--mnist_classifier', help='MNIST classifier pkl.', required=True)
	parser.add_argument('-d', '--decoder', help='Decoder pkl.')
	parser.add_argument('-e', '--encoder', help='Encoder pkl.')
	parser.add_argument('-t', '--transcoder', help='Transcoder pkl.')
	parser.add_argument('-i', '--image_dir', help='Image output directory.')

	args = parser.parse_args()
	main(args.test_data, args.test_data_labels, args.mnist_classifier, args.decoder, args.encoder, args.transcoder, args.image_dir)
