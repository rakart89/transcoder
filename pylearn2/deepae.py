from pylearn2.models.mlp import Layer, MLP, PretrainedLayer
from pylearn2.costs.autoencoder import GSNFriendlyCost
from pylearn2.utils import wraps
from pylearn2.space import VectorSpace
from theano import tensor, shared
from theano.compat.python2x import OrderedDict
from pylearn2.blocks import Block
import theano
class DeepAutoencoder(MLP, Block):

	def __init__(self, corruptor, **kwargs):
		super(DeepAutoencoder, self).__init__(**kwargs)
		self.corruptor = corruptor	
	
#	def perform(self, inputs):
#		return self.encode(shared(inputs)).eval()
		
	def __call__(self, inputs):
		return self.encode_middle(inputs)
#		return self.encode(inputs)
		
#	def upward_pass(self, inputs):
#		return self.encode(inputs)

#	def perform(self, inputs):
#		return self.encode(shared(inputs)).eval()

	def reconstruct(self, inputs):
		corrupted = self.corruptor(inputs)
		rval = self.fprop(corrupted)
		return rval
#		for layer in self.layers[::-1]:
#			rval = layer.downward_pass(rval)
##			rval = self.reconstruct(rval)
#		return self.fprop(inputs)
	
	def reconstruct_from_h(self, inputs):
		rval = inputs
		for layer in self.layers[len(self.layers)/2:]:
			rval = layer.fprop(rval)
#			rval = self.reconstruct(rval)
		return rval

	def encode(self, inputs):
		rval = self.layers[0].fprop(inputs)
		return rval
#		return self.fprop(X, True)

	def encode_middle(self, inputs):
		rval = inputs
		for layer in self.layers[:len(self.layers)//2]:
			rval = layer.fprop(rval)
		return rval

	def decode(self, inputs):
		rval = self.layers[1].fprop(inputs)
		return rval

	def debug_encode(self, X, Y):
		try:
			print X.eval()
		except: 
			pass
		return (Y).sum(axis=1).mean()

class CompositeMeanCategorical(GSNFriendlyCost):

#	@staticmethod
#	def cost(target, output):
#		indices = [shared(0)]
#		for out in output:
#			indices.append(tensor.sum(indices[-1] + out.shape[1]))
##			indices.append(theano.printing.Print('pippo%d' % i)(tensor.sum(indices[-1] + [out.shape[1]])))
#		return tensor.sum([tensor.nnet.binary_crossentropy(output[o], target[:, indices[o]:indices[o+1]]).sum(axis=1).mean() for o in range(len(output))])

	@staticmethod
	def cost(target, output):
		indices = [shared(0)]
		costs = []
		for out in output:
			indices.append(tensor.sum(indices[-1] + out.shape[1]))
			single_target = target[:, indices[-2]:indices[-1]]
			costs.append((-tensor.mul(single_target, tensor.log(out))).sum(axis=1).mean())
#			costs.append(-tensor.log(out[tensor.argmax(single_target, axis=1)]).mean())
#			costs.append(tensor.nnet.binary_crossentropy(out, single_target).sum(axis=1).mean())
		return tensor.sum(costs)

class CompositeMeanSquaredResconstructionError(GSNFriendlyCost):

	@staticmethod
	def cost(target, output): 
		indices = [shared(0)]
		costs = []
		for out in output:
			indices.append(tensor.sum(indices[-1] + out.shape[1]))
			single_target = target[:, indices[-2]:indices[-1]]
			cost = ((single_target - out) ** 2).sum(axis=1).mean()
			costs.append(cost)

		return tensor.sum(costs)

class MeanCategoricalCrossEntropy(GSNFriendlyCost):
	@staticmethod
	def cost(target, output):
		return -tensor.sum(target*tensor.log(output), axis=1).mean()
                        

class PretrainedTransposedLayer(Layer):
	"""
	A layer whose weights are initialized, and optionally fixed,
	based on prior training and then transposed.

	Parameters
	----------
	layer_content : Model
		  Should implement "upward_pass" (RBM and Autoencoder do this)
	freeze_params: bool
		  If True, regard layer_conent's parameters as fixed
		  If False, they become parameters of this layer and can be
		  fine-tuned to optimize the MLP's cost function.
	"""
	
	def __init__(self, layer_name, layer_content, freeze_params=False):
		super(PretrainedTransposedLayer, self).__init__()
		self.__dict__.update(locals())
		del self.self

	@wraps(Layer.set_input_space)
	def set_input_space(self, space):

		assert self.get_input_space() == space
	
	@wraps(Layer.get_params)
	def get_params(self):
		if self.freeze_params:
			return []
		return self.layer_content.get_params()

	@wraps(Layer.get_input_space)
	def get_input_space(self):

		return self.layer_content.get_output_space()

	@wraps(Layer.get_output_space)
	def get_output_space(self):
		
		return self.layer_content.get_input_space()

	@wraps(Layer.get_monitoring_channels)
	def get_monitoring_channels(self):
		warnings.warn("Layer.get_monitoring_channels is " + \
									"deprecated. Use get_layer_monitoring_channels " + \
									"instead. Layer.get_monitoring_channels " + \
									"will be removed on or after september 24th 2014", stacklevel=2)
		
		return OrderedDict([])

	@wraps(Layer.get_layer_monitoring_channels)
	def get_layer_monitoring_channels(self, state_below=None,
																		state=None, targets=None):
		return OrderedDict([])

	@wraps(Layer.fprop)
	def fprop(self, state_below):
#		if len(self.get_params()) == 3:
		return self.layer_content.mean_v_given_h(state_below) #FOR RBM
#		else:
#			return self.layer_content.upward_pass(state_below)

	def downward_pass(self, state_below):
		return state_below

class PretrainedHiddenLayer(Layer):
	"""
	A layer whose weights are initialized, and optionally fixed,
	based on prior training and then transposed.

	Parameters
	----------
	layer_content : Model
		  Should implement "upward_pass" (RBM and Autoencoder do this)
	freeze_params: bool
		  If True, regard layer_conent's parameters as fixed
		  If False, they become parameters of this layer and can be
		  fine-tuned to optimize the MLP's cost function.
	"""
	
	def __init__(self, layer_name, layer_content, dec=False, freeze_params=False):
		super(PretrainedHiddenLayer, self).__init__()
		self.__dict__.update(locals())
		del self.self
		self.dec = dec

	@wraps(Layer.set_input_space)
	def set_input_space(self, space):

		assert self.get_input_space() == space
	
	@wraps(Layer.get_params)
	def get_params(self):
		if self.freeze_params:
			return []
		return self.layer_content.get_params()

	@wraps(Layer.get_input_space)
	def get_input_space(self):
		if self.dec:
			return self.layer_content.layers[0].get_output_space()
		else:
			return self.layer_content.layers[0].get_input_space()
			

	@wraps(Layer.get_output_space)
	def get_output_space(self):
		if self.dec:
			return self.layer_content.layers[0].get_input_space()
		else:
			return self.layer_content.layers[0].get_output_space()
			

	@wraps(Layer.get_monitoring_channels)
	def get_monitoring_channels(self):
		warnings.warn("Layer.get_monitoring_channels is " + \
									"deprecated. Use get_layer_monitoring_channels " + \
									"instead. Layer.get_monitoring_channels " + \
									"will be removed on or after september 24th 2014", stacklevel=2)
		
		return OrderedDict([])

	@wraps(Layer.get_layer_monitoring_channels)
	def get_layer_monitoring_channels(self, state_below=None,
																		state=None, targets=None):
		return OrderedDict([])

	@wraps(Layer.fprop)
	def fprop(self, state_below):
		if self.dec:
			return self.layer_content.decode(state_below)
		else:
			return self.layer_content.encode(state_below)


	def downward_pass(self, state_below):
		return state_below

