import theano.tensor as T
import theano
from pylearn2.costs.cost import Cost, DefaultDataSpecsMixin
from pylearn2.models.mlp import Layer, MLP
from pylearn2.utils import wraps, serial
import numpy as np
from collections import OrderedDict
from itertools import izip
from pylearn2.blocks import Block

#def distance_matrix(X):
#	x2 = (X ** 2).sum(axis=1).reshape((X.shape[0], 1))  # column vector containing norms of the vectors
#	_2x2 = x2 + x2.T  # yields a matrix where element (i,j) = norm of i + norm of j
#	return distances

def _standardize(X):
	Xmean = X.mean(axis=0)
	Xstd = X.std(axis=0)
#	Xstd[Xstd == 0.0] = 1.0
	return (X - Xmean) / Xstd

class Transcoder(MLP, Block):

	def __init__(self, encoder=None, decoder=None, **kwargs):
		l, r = [], []
		if not encoder is None:
			self.encoder = serial.load(encoder)
			middle_layer_encoder = len(self.encoder.layers)//2-1
			for layer in self.encoder.layers[:middle_layer_encoder]:
				layer.freeze_params = True
				layer.mlp = None
				l.append(layer)

		if not decoder is None:
			self.decoder = serial.load(decoder)
			middle_layer_decoder = len(self.decoder.layers)//2
			for layer in self.decoder.layers[middle_layer_decoder:]:
				layer.freeze_params = True
				layer.mlp = None
				r.append(layer)

		self.encoder_size = len(l)
		self.transcoder_size = len(kwargs['layers'])
		self.decoder_size = len(r)

		kwargs['layers'] = l + kwargs['layers'] + r	
		
		super(Transcoder, self).__init__(**kwargs)

	def __call__(self, inputs):
		return self.transcode(inputs)

	def transcode(self, inputs):
		return self.fprop(inputs, True)[self.encoder_size + self.transcoder_size-1]
		


class TranscoderCost(DefaultDataSpecsMixin, Cost):
	supervised = False

	def distance_matrix(self, X):
        	x2 = (X ** 2).sum(axis=1).reshape((X.shape[0], 1))  # column vector containing norms of the vectors
	        _2x2 = x2 + x2.T  # yields a matrix where element (i,j) = norm of i + norm of j
		distances = _2x2 - 2*X.dot(X.T) # element (i,j) is the distance between i and j
		distances = T.triu(distances , 1)
        	distances = T.maximum(distances, 1e-12)
#		distance = T.extra_ops.fill_diagonal(distances, 0)

		return distances

	def expr(self, model, data, **kwargs):
		space, source = self.get_data_specs(model)
		space.validate(data)
		samples = model.batch_size
		X = data
#		X_hat = model.dropout_fprop(
#			X, 
#			default_input_include_prob=0.5, 
#			default_input_scale=2, 
#			input_include_probs = {'o': 1.0},
#			input_scales = {'o':1.0})

		X_hat = model.fprop(X)
		idist = self.distance_matrix(X)
		odist = self.distance_matrix(X_hat)

		loss = T.sum((T.sqrt(odist) - T.sqrt(idist))**2) / T.sum(idist) 
	        return loss

class TranscoderCost2(DefaultDataSpecsMixin, Cost):
        supervised = False

        def distance_matrix(self, X):
                x2 = T.sqrt((X ** 2).sum(axis=1).reshape((X.shape[0], 1)))  # column vector containing norms of the vectors
                _2x2 = x2 * x2.T  # yields a matrix where element (i,j) = norm of i + norm of j
                distances = (1-X.dot(X.T)) / _2x2 # element (i,j) is the distance between i and j

                return T.maximum(distances, 1e-12)

        def expr(self, model, data, **kwargs):
                space, source = self.get_data_specs(model)
                space.validate(data)
                samples = model.batch_size
                X = data

                X_hat = model.fprop(X)
                idist = self.distance_matrix(X)
                odist = self.distance_matrix(X_hat)

		loss = T.sum(idist + odist - 2*T.sqrt(idist*odist))

#               loss = T.sum((odist - idist)**2) / (X.shape[1]*(X.shape[1]-1))
#                loss = T.max(T.abs_(odist - idist))
#		loss = T.max(T.abs_(odist - idist))
                return loss

class TranscoderCostCross(DefaultDataSpecsMixin, Cost):
	supervised = False

	def batch_cross_entropy(self, X):
		r = 1e-9
	#	cross = -(X.dot(T.log(X.T+r)) + (1-X).dot(T.log(1-X.T+r)))
	#	auto = -(X*T.log(X+r) + (1-X)*T.log(1-X+r))
	#	return (cross.sum(axis=1) - auto.sum(axis=1)) / (X.shape[0]-1)	
	#	cross = -(X.dot(T.log(X.T+r)) + (1-X).dot(T.log(1-X.T+r)))
		cross = -(X.dot(T.log(X.T + r)) + (1-X).dot(T.log(1-X.T + r)))
		cross = T.triu(cross , 1)
        	cross = T.maximum(cross, 0)

		return cross

	def expr(self, model, data, **kwargs):
		space, source = self.get_data_specs(model)
		space.validate(data)
		samples = model.batch_size
		X = data
		X_hat = model.fprop(X)

		idist = self.batch_cross_entropy(X)
		odist = self.batch_cross_entropy(X_hat)

		loss = T.sum((odist - idist)**2) / T.sum(idist**2)

#		loss = T.mean(T.nnet.binary_crossentropy(odist+1e-9, idist+1e-9))
		return loss

class TranscoderCostCross2(DefaultDataSpecsMixin, Cost):
	supervised = False

	def batch_cross_entropy(self, X):
		r = 1e-9
	#	cross = -(X.dot(T.log(X.T+r)) + (1-X).dot(T.log(1-X.T+r)))
	#	auto = -(X*T.log(X+r) + (1-X)*T.log(1-X+r))
	#	return (cross.sum(axis=1) - auto.sum(axis=1)) / (X.shape[0]-1)	
	#	cross = -(X.dot(T.log(X.T+r)) + (1-X).dot(T.log(1-X.T+r)))
		cross = -(X.dot(T.log(X.T + r)) + (1-X).dot(T.log(1-X.T + r)))
		cross = T.extra_ops.fill_diagonal(cross, 0)
		cross = T.maximum(cross, 0)

		return cross

	def expr(self, model, data, **kwargs):
		space, source = self.get_data_specs(model)
		space.validate(data)
		samples = model.batch_size
		X = data
		X_hat = model.fprop(X)

		idist = self.batch_cross_entropy(X)
		odist = self.batch_cross_entropy(X_hat)

#		loss = T.sum((odist - idist)**2) / T.sum(idist**2)
		loss = T.max(T.abs_(odist - idist))
#		loss = T.mean(T.nnet.binary_crossentropy(odist+1e-9, idist+1e-9))
		return loss
