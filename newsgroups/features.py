from sklearn.datasets import fetch_20newsgroups
from nltk import wordpunct_tokenize
from nltk import PorterStemmer
from nltk.corpus import names, stopwords, words, reuters
from nltk.probability import *
import numpy as np
import argparse

def find_top_categories(top=10): 
	remove=('headers', 'footers', 'quotes')
	newsgroups_train = fetch_20newsgroups(subset='train')
	categories_frequency = FreqDist(newsgroups_train.target)
	sorted_c = sorted([[c, categories_frequency[c]] for c in categories_frequency], key=lambda x:x[1],reverse=True)
	categories = [newsgroups_train.target_names[w[0]] for w in sorted_c[:top]]	
#	return categories, {c[0]:i for c,i in zip(sorted_c, range(len(sorted_c)))}
	return categories


def find_most_frequent_words(newsgroups_train, top=2000):
	sw = stopwords.words('english')
	porter = PorterStemmer()
	words = []
	for sentence in newsgroups_train.data:
		words += [porter.stem(word.lower()) for word in wordpunct_tokenize(sentence) 
		if word.isalpha() and word.lower() not in sw]

	words_frequency = FreqDist(words)
	sorted_f = sorted([[w, words_frequency[w]] for w in words_frequency], 
		key=lambda x:x[1],reverse=True)[:top]

	return {w[0]:i for w,i in zip(sorted_f, range(len(sorted_f)))}

def get_design_matrix(task='training', top_w=2000, top_c=10, bernoulli=False):
	sw = stopwords.words('english')
	porter = PorterStemmer()

	top_categories = find_top_categories(top_c)
	newsgroups_train = fetch_20newsgroups(subset='train', categories = top_categories)
	top_words = find_most_frequent_words(newsgroups_train, top_w)

	if task == 'training':	
		raw = newsgroups_train
	elif task == 'test':
		raw = fetch_20newsgroups(subset='test', categories = top_categories)
	else:
		return None

	X = np.zeros((len(raw.data), top_w))
	y = raw.target

	for sentence,i in zip(raw.data, range(len(raw.data))):
		feature_vector = FreqDist([porter.stem(word.lower()) 
			for word in wordpunct_tokenize(sentence) 
			if porter.stem(word.lower()) in top_words])

		sum_f_v = sum(feature_vector.values())
		for f in feature_vector:
			#X[i, top_words[f]] = 1.*feature_vector[f]/sum_f_v
			X[i, top_words[f]] = feature_vector[f]
			
	if bernoulli:
		return 1.*(X > np.zeros(X.shape)), y

	return X, y

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Extract either binary or counts bag of words from Newsgroups.')
	parser.add_argument('-tc', '--top_cat', help='Select top categories. Default 10.', default=10, type=int)
	parser.add_argument('-tw', '--top_words', help='Select top frequent words. Default 2000.', default=2000, type=int)
	parser.add_argument('-b', '--bernoulli', help='If set extract binary features else counts features.', action='store_true')
	parser.add_argument('-f', '--folder', help='Output folder.', default='.')
	args = parser.parse_args()
	tasks = {'training':'train', 'test':'test'}
        top_c = args.top_cat
	top_w = args.top_words
	bernoulli = args.bernoulli
	folder = args.folder

	print top_c, top_w, bernoulli

	bernoulli = True
	for task in tasks:
		print 'Processing %s...' % task
		X, y = get_design_matrix(task=task, top_w=top_w, top_c=top_c, bernoulli=bernoulli)
		print 'Saving %s' % task
		np.save('%s/X_%s' % (folder, tasks[task]), X)
		np.save('%s/y_%s' % (folder, tasks[task]), y)
